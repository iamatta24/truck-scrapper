require("dotenv-flow").config();
require("module-alias/register");
const express = require("express");
const app = express();
const router = require("./routes");
const cors = require("cors");
const bodyParser = require("body-parser");
app.set('view engine', 'ejs');
app.set('views', './src/views');
const { gotScraping } = require('got-scraping');
const cheerio = require('cheerio');
const fs = require('fs');
const json2csv = require('json2csv');

app.get('/', async (req, res) => {
  res.render('pages/index');
});

app.get('/submit', async (req, res) => {
  // console.log(req.params)
  console.log(req.query)
  // console.log(req.body)
  const { name } = req.query
  const result = await scrapData(`${name.trim()}`)
  res.send(result)

});

app.get('/company-detail', function (req, res) {
  res.render('pages/company-detail');
});
const listenPort = 3000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.use(cors());
app.use(router);

app.listen(listenPort, function () {
  console.log(`Hi your appp is now up on port ${listenPort} with environment`);
});



const scrapData = async (hitURl) => {

  const fields = ['Name',
    'Address',
    'Phone',
    'USDOT Number',
    'Safety Rating',
    'Cargo Types',
    'Fleet Size',
    'MCS-150 Mileage',
    'MCS-150 Mileage Year',
    'Total Trucks',
    'Total Drivers'];

  const baseUrl = 'https://www.truckdriver.com/trucking-company-directory/'
  try {
    const response = await gotScraping(hitURl);

    const html = response.body;
    const $ = cheerio.load(html);
    const companiesDetail = []

    const pages = [{
      url: `ShowDOTCoByState.cfm?PHY_NATN=${getCountry(hitURl)}&PHY_ST=${getState(hitURl)}&vcAStart=${getStart(hitURl)}&vcAEnd=${getEnd(hitURl)}&SR=0&MT=2`,
      pageNo: 1,
      id: 1
    },]
    const companies = [];
    $(`.text-center [href*='vcAStart=${getStart(hitURl)}&vcAEnd=${getEnd(hitURl)}']`).each((index, value) => {
      const getPageNo = $(value).text();
      if (!(isNaN(parseInt(getPageNo)))) pages.push({ "url": $(value).attr('href'), pageNo: parseInt(getPageNo), id: index + 2 })
    });


    const checkRateLimit = pages.length * 50
    if (checkRateLimit > 1000) return { 'error': 'Scrapper limit exceeded, Your requested data is too large. The maximum allowed limit is 1000 and you are requested for ' + checkRateLimit + ' So, Please Reduce vcAEnd. ' }

    pages.forEach((page) => companies.push(getCompanies(page.url, page.pageNo, baseUrl, companiesDetail)))
    const final = await Promise.all(companies)
    console.dir(final.flat())
    const states = final.flat()
    return states
  } catch (error) {
    return { 'error': 'Something went wrong! The URL is not correct. URL should like \n \n https://www.truckdriver.com/trucking-company-directory/ShowDOTCoByState.cfm?PHY_NATN=US&PHY_ST=AL&vcAStart=A&vcAEnd=A&SR=1&MT=2' }
  }
}
const getState = (url) => {
  const myArray = url.split("PHY_ST=");
  let state = myArray[1].split("&");
  return state[0];
}
const getCountry = (url) => {
  const myArray = url.split("PHY_NATN=");
  let country = myArray[1].split("&");
  return country[0];
}
const getStart = (url) => {
  const myArray = url.split("vcAStart=");
  // console.log(myArray[1].split("&"))
  let state = myArray[1].split("&");
  return state[0];
}
const getEnd = (url) => {
  const myArray = url.split("vcAEnd=");
  let state = myArray[1].split("&");
  return state[0];
}

const getCompanies = async (url, pageNo, baseUrl, companiesDetail) => {
  console.log(`=====> GOING TO SRACP COMPANY FOR THIS PAGE ${pageNo} `)
  const response = await gotScraping(`${baseUrl}${url}`);
  const html = response.body;
  console.log(html);
  const $ = cheerio.load(html);
  const extractCompanies = []
  const companies = []
  $("[href*='ShowDOTCo.cfm?CENSUS_NUM']").each((index, value) => {
    extractCompanies.push({ "url": baseUrl + $(value).attr('href'), company: $(value).text() });
  });


  extractCompanies.forEach((company) => companies.push(getCompanyDetail(company.url)))
  const final = await Promise.all(companies)
  const output = final.flat()
  return output
}

const getIndex = (extractCompanyDetail) => {
  return extractCompanyDetail.toArray().findIndex(x => x.data.trim().includes('Fleet Size: '))
}

const getIndexDriver = (extractCompanyDetail) => {
  return extractCompanyDetail.toArray().findIndex(x => x.data.trim().includes('Last Update for this record: '))
}
const createCargo = (extractCompanyDetail) => {
  const getNValue = getIndex(extractCompanyDetail)
  let cargo = '';
  for (var n = 3; n < getNValue; n++) {
    cargo += `${extractCompanyDetail[`${n}`].data.trim()}${n !== (getNValue - 1) ? ', ' : ''}`
  }
  return cargo.replace(/\s+/g, ' ').trim()
}

const createFleetInfo = (extractCompanyDetail) => {

  const detaultColumns = [];
  detaultColumns['Fleet Size'] = 'N/A';
  detaultColumns['MCS-150 Mileage'] = 'N/A';
  detaultColumns['MCS-150 Mileage Year'] = 'N/A';
  detaultColumns['Total Trucks'] = 'N/A';
  detaultColumns['Total Drivers'] = 'N/A';

  const start = getIndex(extractCompanyDetail)
  const getNValue = getIndexDriver(extractCompanyDetail)
  for (var n = start; n < getNValue; n++) {
    const data = extractCompanyDetail[`${n}`].data.trim().split(':')
    detaultColumns[`${data[0].trim()}`] = `${data[1].trim().replace(/,/g, '')}`
  }
  return detaultColumns
}


const getCompanyDetail = async (url) => {
  console.log(`=====> GOING TO SRACP COMPANY FOR THIS PAGE ${url}`)
  const response = await gotScraping(`${url}`);
  const html = response.body;
  // console.log(html);
  const $ = cheerio.load(html);
  let companyDetial = {}
  const contents = [...$('#dot-truck-co-profile').contents()]
  const extractCompanyDetail = $(contents.filter((e, i) => (e.type === "text" && (contents[i - 1]?.tagName === "br" || contents[i - 1]?.tagName === "h3") && e.data.trim() !== "") && (e.data = e.data.trim())))
  // const getFleetIndex = await getIndex(extractCompanyDetail)
  companyDetial['Url'] = url;
  companyDetial['Name'] = $('#dot-truck-co-profile strong em').text().trim().replace(/,/g, '');
  companyDetial['Address'] = `${extractCompanyDetail['0'].data} , ${extractCompanyDetail['1'].data}`.replace(/,/g, '')
  companyDetial['Phone'] = $('#dot-truck-co-profile strong:eq(1)').text().replace("Phone: ", "").trim().replace(/,/g, '')
  companyDetial['USDOT Number'] = $('#dot-truck-co-profile a:eq(0)').text().trim().replace(/,/g, '')
  companyDetial['Safety Rating'] = extractCompanyDetail['2'] && extractCompanyDetail['2'].data.trim().replace(/,/g, '')
  companyDetial['Cargo Types'] = createCargo(extractCompanyDetail).replace(/,/g, ' | ')
  // const start = getIndex(extractCompanyDetail)
  // const getNValue = getIndexDriver(extractCompanyDetail)

  // for (var n = start; n < getNValue; n++) {
  //   companyDetial[`${n}`] = `${extractCompanyDetail[`${n}`]}`.trim().replace(/,/g, '')
  // }
  return { ...companyDetial, ...createFleetInfo(extractCompanyDetail) }
  // companyDetial['Fleet Size'] = extractCompanyDetail[`${getFleetIndex}`] && extractCompanyDetail[`${getFleetIndex}`].data.replace("Fleet Size: ", "").trim().replace(/,/g, '')
  // companyDetial['MCS-150 Mileage'] = extractCompanyDetail[`${getFleetIndex + 1}`] && extractCompanyDetail[`${getFleetIndex + 1}`].data.replace("MCS-150 Mileage: ", "").trim().replace(/,/g, '')
  // companyDetial['MCS-150 Mileage Year'] = extractCompanyDetail[`${getFleetIndex + 2}`] && extractCompanyDetail[`${getFleetIndex + 2}`].data.replace("MCS-150 Mileage Year: ", "").trim().replace(/,/g, '')
  // companyDetial['Total Trucks'] = extractCompanyDetail[`${getFleetIndex + 3}`] && extractCompanyDetail[`${getFleetIndex + 3}`].data.replace("Total Trucks: ", "").trim().replace(/,/g, '')
  // companyDetial['Total Drivers'] = extractCompanyDetail[`${getFleetIndex + 4}`] && extractCompanyDetail[`${getFleetIndex + 4}`].data.replace("Total Drivers: ", "").trim().replace(/,/g, '')
  // companyDetial.name = extractCompanyDetail['14'].data.replace("Fleet Size: ", "").trim()
  // companyDetial.name = extractCompanyDetail['2'].data.replace("Fleet Size: ", "").trim()

  // $("[href*='ShowDOTCo.cfm?CENSUS_NUM']").each((index, value) => {
  //   extractCompanies.push({ "url": baseUrl + $(value).attr('href'), company: $(value).text() });
  // });
  // return companyDetial
}


