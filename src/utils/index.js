const e = require("express");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const jsonwebtoken = require("jsonwebtoken");
const referralCodes = require("voucher-code-generator");
const getReactBaseUrl = {
  development: {
    url: process.env.REACT_APP_URL_STAGING,
  },
  local: {
    url: process.env.REACT_APP_URL_DEVELOPMENT,
  },
  production: {
    url: process.env.REACT_APP_URL_PRODUCTION,
  },
};

// const apiResponse = (message, body, status, res, datakey = "data") => {
//   if (status == 500)
//     res.status(status).send({
//       isSuccess: false,
//       message: message,
//       error_description: body ? body : "",
//     });
//   return res.status(200).send({
//     isSuccess: status == "200" ? true : false,
//     message: message,
//     data: body ? body : "",
//   });
// };
const makeUniqueString = (length) => {
  let result = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};
const decodeAuthToken = (token) => {
  const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
  return decodedToken.data;
};

module.exports = {
 
};
