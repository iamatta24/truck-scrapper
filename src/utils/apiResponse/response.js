"use strict";

const checkStatusCode = require("./helper");

const success = (res, message = false, data = null) => {
  return res.status(200).json({
    statusCode: 200,
    error: false,
    data: data || null,
    message: message || "OK",
  });
};

const badRequest = (res, message = false, errors = false, data = null) => {
  return res.status(400).json({
    statusCode: 400,
    error: true,
    data: data || null,
    message: message || "Bad Request, Something went",
    errors: errors || false,
  });
};

const create = (res, message = false, data = null) => {
  if (!statusCode) throw new Error("Status code is required");
  if (isNaN(Number(statusCode))) throw new Error("Status code not a number");

  return res.status(statusCode).json({
    statusCode: 200,
    error: error || null,
    data: data || null,
    message: checkStatusCode(statusCode, message),
    errors: errors || false,
  });
};

const unAuthorized = (res, message = false, data = null) => {
  return res.status(401).json({
    statusCode: 401,
    error: true,
    isAuthorized: false,
    data: data || null,
    message:
      message ||
      "Unauthorized request, Please use different token or login again.",
  });
};

const internalError = (res, message = false, data = null) => {
  return res.status(500).json({
    statusCode: 500,
    error: true,
    data: data || null,
    message: message || "Internal Server Error",
  });
};
module.exports = {
  success,
  internalError,
  unAuthorized,
  create,
  badRequest,
};
// (module.exports =
//     success,
//     badRequest,
//     {

//   unAuthorized: (res, message = false, data = null) => {
//     this.statusCode = 401;
//     this.error = true;
//     this.isAuthorized = false;
//     this.data = data || null;
//     this.message =
//       message ||
//       "Unauthorized request, Please use different token or login again.";

//     return res.status(this.statusCode).json(this);
//   },

//   forbidden: (res, message = false, data = null) => {
//     this.statusCode = 403;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Forbidden";

//     return res.status(this.statusCode).json(this);
//   },

//   notFound: (res, message = false, data = null) => {
//     this.statusCode = 404;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Not Found";

//     return res.status(this.statusCode).json(this);
//   },

//   notAllowed: (res, message = false, data = null) => {
//     this.statusCode = 405;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Method Not Allowed";

//     return res.status(this.statusCode).json(this);
//   },

//   requestTimeout: (res, message = false, data = null) => {
//     this.statusCode = 408;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Request Timeout";

//     return res.status(this.statusCode).json(this);
//   },

//   internalError: (res, message = false, data = null) => {
//     this.statusCode = 500;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Internal Server Error";

//     return res.status(this.statusCode).json(this);
//   },

//   badGateway: (res, message = false, data = null) => {
//     this.statusCode = 502;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Bad Gateway";

//     return res.status(this.statusCode).json(this);
//   },

//   unavailable: (res, message = false, data = null) => {
//     this.statusCode = 503;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Service Unavai­lable";

//     return res.status(this.statusCode).json(this);
//   },

//   gatewayTimeout: (res, message = false, data = null) => {
//     this.statusCode = 504;
//     this.error = true;
//     this.data = data || null;
//     this.message = message || "Gateway Timeout";

//     return res.status(this.statusCode).json(this);
//   },
// });
