"use strict";
const {
  success,
  internalError,
  unAuthorized,
  create,
  badRequest,
} = require("./response");
/**
 * Export default singleton.
 *
 * @api public
 */
module.exports = {
  success,
  internalError,
  unAuthorized,
  create,
  badRequest,
};
