const router = require("express").Router();
const authRoutes = require("./auth");
const publicRoutes = require("./public");
const { QueryTypes } = require("sequelize");
const models = require("@models");

// ____________________________

const {
  fetchSearchCompany,fetchCompanyDetail
} = require("@controllers");


/**********************************
 *
 * AUTH ROUTES
 *
 ************************************/

// Here you will write the Auth Base Route  EG: router.use("/auth", authRoutes);
router.use("/auth", authRoutes);

/**********************************
 *
 * PUBLIC ROUTES
 *
 ************************************/
router.use("/public", publicRoutes);

router.get("/search/:search", fetchSearchCompany);
router.get("/detail/:search", fetchCompanyDetail);



module.exports = router;
