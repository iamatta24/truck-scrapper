var spinner = $("#loader");
$(function () {
  $("form").submit(function (e) {
    e.preventDefault();
    spinner.show();
    value = $("#txt_name").val();
    console.log(value);
    // const url = "https://jsonplaceholder.typicode.com/todos/1";
    $.get(value, function (response) {
      console.log(response);
      spinner.hide();
      value = $("#txt_name").val("");
    }).fail(function (err) {
      console.error(err);
      spinner.hide();
    });
  });
});
