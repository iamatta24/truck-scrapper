const _ = require("lodash");
const db = require("@models/index").sequelize;
const { QueryTypes } = require("sequelize");
let { success, badRequest, internalError } = require("@utils/apiResponse");

String.prototype.isNumber = function(){return /^\d+$/.test(this);}
/**********************************
 *
 * FETCH SEARCHED COMPANIES
 *
 ************************************/
const fetchSearchCompany = async ({ params }, res) => {
  try {
      const getCompany= await db.query(
    `SELECT organization_name, id  FROM crunchs WHERE organization_name LIKE '${params.search}%' ORDER BY  id ASC `,
    { type: QueryTypes.SELECT, raw: true}
  );
    return success(res, "Company fetched successfully ", {count: getCompany.length,companies:getCompany });
  } catch (error) {
    return internalError(res, error.message);
  }
};


/**********************************
 *
 * FETCH  COMPANIES DETIAL
 *
 ************************************/
const fetchCompanyDetail = async ({ params }, res) => {
  try {
      let id, name;
    let search =params.search
    search.isNumber() ? id = search : name = search
      const queryCondition = id !== undefined? `id='${id}'`: `organization_name ='${name}'`
      const getDetail= await db.query(
    `SELECT * FROM crunchs WHERE ${queryCondition}`,
    { type: QueryTypes.SELECT, raw: true}
  );
    return success(res, "Company detail fetched successfully ", getDetail);
  } catch (error) {
    return internalError(res, error.message);
  }
};


module.exports = {
  fetchSearchCompany,
  fetchCompanyDetail
};