const {
  fetchSearchCompany,
fetchCompanyDetail
} = require("./common/CompanyController");

module.exports = {
  fetchSearchCompany,
fetchCompanyDetail
};
