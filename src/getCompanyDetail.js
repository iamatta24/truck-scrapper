const { gotScraping } = require( 'got-scraping');
const cheerio = require('cheerio');
const getIndex = (extractCompanyDetail) => {
  return extractCompanyDetail.toArray().findIndex(x => x.data.trim().includes('Fleet Size: '))
}


const createCargo = (extractCompanyDetail) => {
  const getNValue = getIndex(extractCompanyDetail)
  let cargo = '';
  for (var n = 3; n < getNValue; n++) {
    cargo += `${extractCompanyDetail[`${n}`].data.trim()}${n !== (getNValue - 1) ? ', ' : ''}`
  }
  return cargo
}

const getCompanyDetail= async(url) => {
  console.log(`=====> GOING TO SRACP COMPANY FOR THIS PAGE ${url}`)
  const response = await gotScraping(`${url}`);
  const html = response.body;
  console.log(html);
  const $ = cheerio.load(html);
  const companyDetial = {}
  const contents = [...$('#dot-truck-co-profile').contents()]
  const extractCompanyDetail = $(contents.filter((e, i) => (e.type === "text" && (contents[i - 1]?.tagName === "br" || contents[i - 1]?.tagName === "h3") && e.data.trim() !== "") && (e.data = e.data.trim())))
  const getFleetIndex = await getIndex(extractCompanyDetail)
  companyDetial['Name'] = $('#dot-truck-co-profile strong em').text().trim();
  companyDetial['Address'] = `${extractCompanyDetail['0'].data} , ${extractCompanyDetail['1'].data}`
  companyDetial['Phone'] = $('#dot-truck-co-profile strong:eq(1)').text().replace("Phone: ", "").trim()
  companyDetial['USDOT Number'] = $('#dot-truck-co-profile a:eq(0)').text().trim()
  companyDetial['Safety Rating'] = extractCompanyDetail['2'] && extractCompanyDetail['2'].data.trim()
  companyDetial['Cargo Types'] = createCargo(extractCompanyDetail)
  companyDetial['Fleet Size'] = extractCompanyDetail[`${getFleetIndex}`] && extractCompanyDetail[`${getFleetIndex}`].data.replace("Fleet Size: ", "").trim()
  companyDetial['MCS-150 Mileage'] = extractCompanyDetail[`${getFleetIndex + 1}`] && extractCompanyDetail[`${getFleetIndex + 1}`].data.replace("MCS-150 Mileage: ", "").trim()
  companyDetial['MCS-150 Mileage Year'] = extractCompanyDetail[`${getFleetIndex + 2}`] && extractCompanyDetail[`${getFleetIndex + 2}`].data.replace("MCS-150 Mileage Year: ", "").trim()
  companyDetial['Total Trucks'] = extractCompanyDetail[`${getFleetIndex + 3}`] && extractCompanyDetail[`${getFleetIndex + 3}`].data.replace("Total Trucks: ", "").trim()
  companyDetial['Total Drivers'] = extractCompanyDetail[`${getFleetIndex + 4}`] && extractCompanyDetail[`${getFleetIndex + 4}`].data.replace("Total Drivers: ", "").trim()
  // companyDetial.name = extractCompanyDetail['14'].data.replace("Fleet Size: ", "").trim()
  // companyDetial.name = extractCompanyDetail['2'].data.replace("Fleet Size: ", "").trim()

  // $("[href*='ShowDOTCo.cfm?CENSUS_NUM']").each((index, value) => {
  //   extractCompanies.push({ "url": baseUrl + $(value).attr('href'), company: $(value).text() });
  // });
  return companyDetial
}
exports.module = {
  getCompanyDetail
}