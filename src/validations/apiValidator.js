const Joi = require("joi");
const PasswordComplexity = require("joi-password-complexity");

const checkZipCodeValidator = Joi.object({
    state_fips: Joi.string().required(),
    zipcode: Joi.string().required(),

});
const getZipCodeValidator = Joi.object({
    state_fips: Joi.string().required(),

});
module.exports = {
getZipCodeValidator,
checkZipCodeValidator
};
