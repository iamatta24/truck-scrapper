const Joi = require('joi')

const AuthorizationTokenRequired = Joi.object({
    authorization: Joi.string().required(),
}).options({ allowUnknown: true })
module.exports = { AuthorizationTokenRequired}
